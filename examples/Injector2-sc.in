// Injector2-sc template file
// started 21-01-16: Nate Pogue based on work of A. Kolano and A. Adelmann
//
//---------------------------------------------------------------------------------------------//
//                                      Global Options                                         //
//---------------------------------------------------------------------------------------------//
OPTION, VERSION        =20200;      // Version number, acknowledging recent changes
OPTION, ECHO           =false;      // If true, command prints an echo of the input files on
                                    // the standard error file.
OPTION, PSDUMPFREQ     =1000000;    // Defines after how many steps the phase is dumped into
                                    // the H5hut file, Default is 10. A higher number dumps
                                    // less. if 1 then every step, every turn. If too low it
                                    // will be time prohibitive, and large file sizes.
OPTION, STATDUMPFREQ   =8;          // Defines after how many steps the bunch statistics is
                                    // dumped into *.stat file. The behaviour is like
                                    // PSDUMPFREQ.
OPTION, SPTDUMPFREQ    =10;         // Defines after how many steps we dump the phase space
                                    // for Single Particle simulations. It is always useful
                                    // record the trajectory of the reference particle or
                                    // some specified particle for a primary study.
                                    // Default value is 1. same as above but single particle
OPTION, REPARTFREQ     =10;         // Defines after how many time steps the particles are
                                    // repartitioned to balance the computational load on
                                    // the computer nodes, Default value is 10.
OPTION, PSDUMPFRAME    =bunch_mean; // Determines whether the phase space data is dumped in
                                    // global frame or in the local frame. If true, run can
                                    // not be restarted as it is in local frame. Default is
                                    // false
OPTION, PSDUMPEACHTURN =true;       // Phase space dumped each turn
OPTION, ENABLEHDF5     =false;      // Enable HDF5 (.h5) output
OPTION, ASCIIDUMP      =true;      // Dump in ascii instead of HDF5
OPTION, CLOTUNEONLY    =false;      // Compute tunes using the matched distribution
IF (false) {
  OPTION, AMR              =false;         // Enable AMR simulation
  OPTION, AMR_YT_DUMP_FREQ =1000000000000; // Defines after how many steps we dump a Yt file
  OPTION, AMR_REGRID_FREQ  =10;            // Defines after how many steps a AMR regrid is performed
}
OPTION, SCSOLVEFREQ    =1;          // Defines after how many steps we compute space-charge
OPTION, SEED           =123456789;  // RNG seed for particle generation

//---------------------------------------------------------------------------------------------//
//                                      Physical Parameters                                    //
//---------------------------------------------------------------------------------------------//
REAL Ebeam     = 0.0008685;                      // injection energy of the beam
REAL gamma     = (Ebeam+PMASS)/PMASS;            // relativistic gamma
REAL beta      = sqrt(1-(1/gamma^2));            // relativistic beta
REAL gammabeta = gamma*beta;                     // self explanatory
REAL P0        = PMASS*gammabeta;                // Initial momentum of the particle
REAL Brho      = (PMASS*1.0e9*gammabeta)/CLIGHT; // Magnetic rigidity

VALUE,{gamma,beta,gammabeta,Ebeam,Brho,CLIGHT};  // Prints values in output to check
                                                 // initial conditions

//---------------------------------------------------------------------------------------------//
//                                      Cyclotron Configuration                                //
//---------------------------------------------------------------------------------------------//
// Below are the series of value and commands required to set up the cyclotron's configuration

INJECTOR2: CYCLOTRON, TYPE=RING, CYHARMON=10, PHIINIT=30.0, PRINIT=-0.00550008,
           RINIT=392.0, SYMMETRY=1.0, RFFREQ=50.6370, BSCALE=1, FMAPFN="Fieldmaps/ZYKL9Z.NAR";

// Note: CYHARMON - Cyclotron's harmonics, SYMMETRY - symmetry of cyclotron, in this case the
//       field map imported is for the whole cyclotron, hence symmetry 1, FMAPFN - insert
//       field maps - in this case the sector dipole field map.
// Note: In general there should be gap widths, otherwise no transit is taken into account. In
//       this model none are specified by one needs to add this for accurate models.

RFM11: RFCAVITY, VOLT=.303619, FMAPFN="Fieldmaps/Cav1.dat", TYPE=SINGLEGAP, FREQ=50.6370,
       RMIN=350.0, RMAX=3150.0, ANGLE=35.0, PDIS=0.0,
       GAPWIDTH=0.0, PHI0=50.0;
RFM12: RFCAVITY, VOLT=.303619, FMAPFN="Fieldmaps/Cav1.dat", TYPE=SINGLEGAP, FREQ=50.6370,
       RMIN=350.0, RMAX=3150.0, ANGLE=55.0, PDIS=0.0,
       GAPWIDTH=0.0, PHI0=((50.0)+200);
RFM21: RFCAVITY, VOLT=.288119, FMAPFN="Fieldmaps/Cav1.dat", TYPE=SINGLEGAP, FREQ=50.6370,
       RMIN=350.0, RMAX=3150.0, ANGLE=((35.0)+180), PDIS=0.0,
       GAPWIDTH=0.0, PHI0=(50.0);
RFM22: RFCAVITY, VOLT=.288119, FMAPFN="Fieldmaps/Cav1.dat", TYPE=SINGLEGAP, FREQ=50.6370,
       RMIN=350.0, RMAX=3150.0, ANGLE=((55.0)+180), PDIS=0.0,
       GAPWIDTH=0.0, PHI0=((50.0)+200);

// Flat Top Cavities

REAL ftfreq=(50.6370)*3;                  // 3rd harmonic frequency for FT Cavity

FT1: RFCAVITY, VOLT=.031, FMAPFN="Fieldmaps/Cav3.dat", TYPE=SINGLEGAP, FREQ=ftfreq,
     RMIN=830.0, RMAX=3150.0, ANGLE=135.0, PDIS=0.0,
     GAPWIDTH=0.0, PHI0=(((50.0)+100)*3);          // 3 comes from 3rd harmonic
FT2: RFCAVITY, VOLT=.031, FMAPFN="Fieldmaps/Cav3.dat", TYPE=SINGLEGAP, FREQ=ftfreq,
     RMIN=830.0, RMAX=3150.0, ANGLE=((135.0)+180), PDIS=0.0,
     GAPWIDTH=0.0, PHI0=(((50.0)+100)*3);          // 3 comes from 3rd harmonic

//---------------------------------------------------------------------------------------------//
//                                      Probes                                                 //
//---------------------------------------------------------------------------------------------//
// RIE1 - extraction probe, contains the last 7 turns, is a wire with stopping block
// RIE2 - vertical extraction probe, contains the last 5 turns, consists of two plates approx
//        15 mm apart with beam going between them. This measures how much the beam deviates
//        from the center.
// RIL1 - Long Probe. This has a full machine scan but only to approximately 2 microamps. At
//        higher energies larger currents are hard to distinguish.
// MXP01- extraction probe, good up to 1 mA, last 4 turns, located after septum, shows the
//        extraction radius.
// RIZ1 - last 13 turns, for time structure measurement

// Probe positioning according to the report of Rudolf Doelling:
// identifier - HIPA-BEAMDY-DR84-023.02-030712
// title      - Wire probe properties in Injector 2 and Ring cyclotron
// date       - 03.07.12
RIZ1:  PROBE, XSTART=-2769.80797259,  XEND=-2982.0814283,   YSTART= 1044.46742649,  YEND= 1256.7408822,   STEP=1.0;
RIE1:  PROBE, XSTART=-2981.184446,    XEND=-3249.523561,    YSTART= 911.4395523,    YEND= 993.4790526,    STEP=1.0;
RIE2:  PROBE, XSTART= 914.240473234,  XEND= 1017.45353244,  YSTART=-2990.3458458,   YEND=-3327.94055075,  STEP=1.0;
RIL1:  PROBE, XSTART= 145.785076437,  XEND= 1029.55562455,  YSTART= 492.162169908,  YEND= 3475.7215389,   STEP=1.0;
MXP01: PROBE, XSTART= 1227.82021485,  XEND= 1296.97525805,  YSTART=-2946.08969314,  YEND=-3015.24473634,  STEP=1.0;

//---------------------------------------------------------------------------------------------//
//                                      Collimators                                            //
//---------------------------------------------------------------------------------------------//
// These values are used to essentially open and close the collimators. Note that they all do
// not scale the same as each collimator has its own angle. To understand get more information
// read the separate collimator file.

// KIP3 - Description needed
REAL KIP3_1L_vary= 23.0;
REAL KIP3_1R_vary= -7.5;
REAL KIP3_2L_vary= 19.4;
REAL KIP3_2R_vary= -13.0;
// KIR1L- Description needed
REAL KIR1L_vary=   -2.8;
REAL KIR1R_vary=   -2.0;
// KIP4 - Description needed
REAL KIP4L_vary=   -27.0;
REAL KIP4R_vary=   38.552;
// KIR3 - Description needed
REAL KIR3L_vary=   -18.93;
REAL KIR3R_vary=   13.47;
// KIV-OU-Description needed
REAL KIVo_vary_Z=  -5.8;
REAL KIVu_vary_Z=  0.0;
// KIP2 - Description needed
REAL KIP2_vary_x=  15.9;
// KIG1 - Description needed
REAL KIG1u_vary_Z= -5.0;
REAL KIG1o_vary_Z= -1.7;
// KIG3 - Description needed
REAL KIG3u_vary_Z= 3.5;
REAL KIG3o_vary_Z= 2.0;

VALUE, {KIP3_1L_vary, KIVU_vary_Z, KIG3U_vary_Z, KIG3O_vary_Z};

//---------------------------------------------------------------------------------------------//
//                                      Distributions                                          //
//---------------------------------------------------------------------------------------------//

// Note: Distributions in eV/c need to be specified as EVOVERC in the parameter INPUTMOUNITS (see Opal
//       Manual). Some from other sources are in BETA*GAMMA, thus have no units.

//For Single Particle
DistSP: DISTRIBUTION, TYPE=fromfile, FNAME="Distribution/spdist.opal", INPUTMOUNITS=EVOVERC;
//For Multiple Particles
DistSC: DISTRIBUTION, TYPE=fromfile, FNAME="Distribution/scdist.opal", INPUTMOUNITS=NONE;
//For Tune 
DistT:  DISTRIBUTION, TYPE=fromfile, FNAME="Distribution/tdist.opal";

//General Distribution
DistG: DISTRIBUTION, TYPE=GAUSS,
       SIGMAX=1.26e-03, SIGMAPX=0, CORRX=0,
       SIGMAY=6.70e-03, SIGMAPY=3.68e-3, CORRY=0,
       SIGMAZ=1.26e-03, SIGMAPZ=0, CORRZ=0,
       R61=0, R62=0, R51=0, R52=0,
       SCALABLE=false;

//---------------------------------------------------------------------------------------------//
//                                      Choice of Simulation                                   //
//---------------------------------------------------------------------------------------------//
REAL RF_MODE   = 0;    // Selects Single Particle mode
REAL SC_MODE   = 1;    // Selects Space Charge mode
REAL TUNE_MODE = 0;    // Selects Tune Calculation
//---------------------------------------------------------------------------------------------//
//                                      Tune Mode - Easiest to Start with.                     //
//---------------------------------------------------------------------------------------------//
// Calculation of the betatron oscillation frequency for a cyclotron. Calls tuning.bash and
// works with FIXPOEORBIT output data.
// Requires a distribution file with two particles: first particle is the reference orbit, the
// second is displaced off center with a dx and dz as follows:  (note dx radial, dz vertical)
// 2
// 0.0  0.0     0.0     0.0     0.0     0.0
// 0.01 0.0     0.0     0.0     0.01    0.0
// When the total number of particles is equal to 2, SEO mode is triggered automatically,
// regardless of the existence of a cavity or other components.

IF (TUNE_MODE>0){

TITLE, STRING="Tune Calculation of Cyclotron";

Line1: LINE=(INJECTOR2);
// the number of particles must match the number of particles stated in the distribution. Thus
// for tune calculations the number is just hard coded here.

REAL numparticles=2;

Dist: DistT;
FS1:  FIELDSOLVER, FSTYPE=NONE, MX=16, MY=16, MT=16,
      PARFFTX=true, PARFFTY=true, PARFFTT=true,
      BCFFTX=open, BCFFTY=open, BCFFTT=open,
      BBOXINCR=5;
}

//---------------------------------------------------------------------------------------------//
//                                      Single Particle Mode                                   //
//---------------------------------------------------------------------------------------------//
// Sends a single particle through the machine. Allows for quick assessment of the system and
// whether it is work properly or as expected.

IF (RF_MODE>0){

TITLE, STRING="Single Particle through the Cyclotron";

Line1: LINE=(INJECTOR2, RFM11, RFM12, RFM21, RFM22, FT1, FT2, RIE1, RIE2, RIZ1, RIL1, MXP01);

//KIP2, KIG3o, KIG3u, KIVo, KIVu, KIP3_1L, KIP3_1R, KIP3_2L, KIP3_2R, KIR1R, KIR1L, KIG1o, KIG1u, KIR3L,
//KIR3R, KIP4L, KIP4R

// the number of particles must match the number of particles stated in the distribution. Thus
// for single particle calculation the number is just hard coded here.

REAL numparticles=1;

Dist: DistSP;
FS1:  FIELDSOLVER, FSTYPE=NONE, MX=16, MY=16, MT=16,
      PARFFTX=true, PARFFTY=true, PARFFTT=true,
      BCFFTX=open, BCFFTY=open, BCFFTT=open,
      BBOXINCR=5;

}
//---------------------------------------------------------------------------------------------//
//                                      Space Charge Mode                                      //
//---------------------------------------------------------------------------------------------//
// Large Multiparticle Runs where Space Charge must be use dot get correct dynamics.

IF (SC_MODE>0){

TITLE, STRING="Space Charge Simulation in the Cyclotron";

REAL Sigmacutter=0;
IF (Sigmacutter > 0){
OPTION, REMOTEPARTDEL=_Sigmacutter_; // Scrapes off the beam larger than the sigma specified
}

Line1: LINE=(INJECTOR2, RFM11, RFM12, RFM21, RFM22, FT1, FT2, RIE1, RIE2, RIZ1, RIL1, MXP01);
//Line1: LINE = (INJECTOR2, RFM11, RFM12, RFM21, RFM22, FT1, FT2, RIE1, RIE2, RIZ1, KIP2, KIG3o, KIG3u,
//               KIVo, KIVu, KIP3_1L, KIP3_1R, KIP3_2L, KIP3_2R, KIR1R, KIR1L, KIG1o, KIG1u, KIR3L,
//               KIR3R, KIP4L, KIP4R, RIL1, MXP01);

// remember the number of particles has to match was is inserted into the distribution.
REAL numparticles=1e5;

Dist: DistSC;  // Or switch to DistG for general distribution.

IF (false) {
FS1:  FIELDSOLVER, FSTYPE=FFT, MX=16, MY=16, MT=16,
      PARFFTX=true, PARFFTY=true, PARFFTT=true,
      BCFFTX=open, BCFFTY=open,	BCFFTT=open,
      BBOXINCR=2,
      AMR_MAXLEVEL=0, AMR_MAXGRIDX=16, AMR_MAXGRIDY=16, AMR_MAXGRIDZ=16,
      AMR_BFX=8, AMR_BFY=8, AMR_BFZ=8, AMR_REFX=2, AMR_REFY=2, AMR_REFZ=2,
      AMR_TAGGING=CHARGE_DENSITY, AMR_SCALING=0.5, AMR_DENSITY=1.0e-14, AMR_MAX_NUM_PART=2,
      AMR_MIN_NUM_PART=5, AMR_DOMAIN_RATIO={1.0, 0.75, 0.75},
      AMR_MG_SMOOTHER=GS, AMR_MG_NSWEEPS=12, AMR_MG_PREC=NONE, AMR_MG_INTERP=PC,
      AMR_MG_NORM=LINF_NORM, AMR_MG_REBALANCE=true, AMR_MG_REUSE=RAP, ITSOLVER=SA,
      AMR_MG_VERBOSE=false;
} ELSE {
FS1:  FIELDSOLVER, FSTYPE=FFT, MX=16, MY=16, MT=16,
      PARFFTX=true, PARFFTY=true, PARFFTT=true,
      BCFFTX=open, BCFFTY=open,	BCFFTT=open,
      BBOXINCR=2;
}
}
//---------------------------------------------------------------------------------------------//
//                                      Tracking and Computation                               //
//---------------------------------------------------------------------------------------------//

Beam1: BEAM, PARTICLE=PROTON, PC=P0, NPART=numparticles, BCURRENT=0.0095, BFREQ=50.6370;

SELECT, LINE=Line1;

REAL nstep1=3600; //dummy for duplicate variable

TRACK, LINE=Line1, BEAM=Beam1, MAXSTEPS=((nstep1)*(83)), STEPSPERTURN=nstep1,
       TIMEINTEGRATOR=RK4;

RUN, METHOD="CYCLOTRON-T", BEAM=Beam1, FIELDSOLVER=FS1, DISTRIBUTION=Dist,
     MBMODE=force, PARAMB=5.0, TURNS=1, MB_BINNING=GAMMA_BINNING, MB_ETA=0.01;

ENDTRACK;
STOP;

